Everything we do, we believe in challenging the status quo, we believe in thinking differently. The way we challenge the status quo is by adding equity to your property, beautifying your property, and relieving you of the stressful process or restoration work. We just happen to be Contractors.

Address: 210 SW Market St, Lee's Summit, MO 64063, USA

Phone: 816-448-8208

Website: https://manninggc.com/
